import React from 'react';
const WalletConnect = () => {
    return (
        <>
            <div className="rn-connect-area rn-section-gapTop">
                <div className="container">
                    <div className="row g mb--50 mb_md--30 mb_sm--30 align-items-center">
                        <div className="col-lg-6" >
                            <h3 className="connect-title">Connect your wallet</h3>
                            <p className="connect-td">Connect with one of available wallet providers or create a new wallet. <a href="#">What is a wallet?</a></p>

                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-6" >
                            <div className="connect-thumbnail">
                                <div className="left-image">
                                    <img src="assets/images/connect/connect-01.jpg" alt="Nft_Profile" />
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="row g-5">
                                <div className="col-xxl-4 col-lg-6 col-md-4 col-12 col-sm-6 col-12" >
                                    <div className="wallet-wrapper">
                                        <div className="inner">
                                            <div className="icon">
                                                <i data-feather="cast"></i>
                                            </div>
                                            <div className="content">
                                                <h4 className="title"><a href="#">Paypal</a></h4>
                                                <p className="description">I throw myself down among the tall.</p>
                                            </div>
                                        </div>
                                        <a className="over-link" href="#"></a>
                                    </div>
                                </div>
                                <div className="col-xxl-4 col-lg-6 col-md-4 col-12 col-sm-6 col-12" >
                                    <div className="wallet-wrapper">
                                        <div className="inner">
                                            <div className="icon">
                                                <i className="color-purple" data-feather="box"></i>
                                            </div>
                                            <div className="content">
                                                <h4 className="title"><a href="#">FireCoin</a></h4>
                                                <p className="description">This is a great deals For cash transfer</p>
                                            </div>
                                        </div>
                                        <a className="over-link" href="#"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default WalletConnect;

