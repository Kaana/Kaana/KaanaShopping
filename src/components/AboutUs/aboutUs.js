
import React from 'react';

const AboutUs = () => {


    return (
        <div>
            <div className="rn-about-banner-area rn-section-gapTop">
                <div className="container mb--30">
                    <div className="row">
                        <div className="col-12">
                            <h2 className="title about-title-m" >Direct Teams.<br />For Your Dadicated Dreams</h2>
                        </div>
                    </div>
                </div>
                <div className="container-fluid about-fluidimg ">
                    <div className="row">
                        <div className="img-wrapper">
                            <div className="bg_image--22 bg_image">
                        
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row g-5">
                        <div className="col-lg-6">
                            <div className="h--100">
                                <div className="rn-about-card mt_dec--50 widge-wrapper rbt-sticky-top-adjust">
                                    <div className="inner">
                                        <h2 className="title" >
                                            Why We Do This
                                        </h2>
                                        <p className="about-disc" >
                                            Our Kaana Shopping .......
                                        </p>
                                        <a href="blog.html" className="btn btn-primary-alta btn-large sal-animate mt--20">See Our Blog</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="rn-about-card transparent-bg">
                                <div className="inner">
                                    <h3 className="title" >
                                        Helping You <br />Grow In Every Stage.
                                    </h3>
                                    <p className="about-disc mb--0" >
                                        Our Kaana Shopping .....
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default AboutUs;