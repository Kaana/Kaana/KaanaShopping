import React from 'react';
import './account.css';

const Account = () => {
    return (
        <>
            <div className="rn-product-area rn-section-gapTop">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-12 x">
                            <h2 className="text-center mb--50">Pavlo Yanovskii </h2>
                            <nav className="product-tab-nav navcenter" >
                                <div className="nav " id="nav-tabs" role="tablist" >
                                    <button className="nav-link active" id="nav-home-main-tab" data-bs-toggle="tab" data-bs-target="#nav-home-main" type="button" role="tab" aria-controls="nav-home-main" aria-selected="true">Home</button>
                                    <button className="nav-link" id="nav-profile-02-tab" data-bs-toggle="tab" data-bs-target="#nav-profile-02" type="button" role="tab" aria-controls="nav-profile-02" aria-selected="false">Personal Info</button>
                                    <button className="nav-link" id="nav-contact-02-tab" data-bs-toggle="tab" data-bs-target="#nav-contact-03" type="button" role="tab" aria-controls="nav-contact-03" aria-selected="false">Data & Personalization</button>
                                    <button className="nav-link" id="nav-contact-02-tab" data-bs-toggle="tab" data-bs-target="#nav-contact-04" type="button" role="tab" aria-controls="nav-contact-04" aria-selected="false">Security</button>
                                    <button className="nav-link" id="nav-contact-03-tab" data-bs-toggle="tab" data-bs-target="#nav-contact-05" type="button" role="tab" aria-controls="nav-contact-05" aria-selected="false">People & Sharing</button>
                                    <button className="nav-link" id="nav-contact-03-tab" data-bs-toggle="tab" data-bs-target="#nav-contact-06" type="button" role="tab" aria-controls="nav-contact-06" aria-selected="false">Payments & Subscriptions</button>
                                </div>
                            </nav>
                            <div className="tab-content" id="nav-tabContens">
                                <div className="tab-pane lg-product_tab-pane lg-product-col-2 fade show active" id="nav-home-main" role="tabpanel" aria-labelledby="nav-home-main-tab">
                                    Home
                                </div>
                                <div className="tab-pane lg-product_tab-pane lg-product-col-2 fade" id="nav-profile-02" role="tabpanel" aria-labelledby="nav-profile-02-tab">
                                    Personal Info
                                </div>
                                <div className="tab-pane lg-product_tab-pane lg-product-col-2 fade" id="nav-contact-03" role="tabpanel" aria-labelledby="nav-contact-03-tab">
                                    Data & Personalization
                                </div>

                                <div className="tab-pane lg-product_tab-pane lg-product-col-2 fade" id="nav-contact-04" role="tabpanel" aria-labelledby="nav-contact-03-tab">
                                    Security
                                </div>
                                <div className="tab-pane lg-product_tab-pane lg-product-col-2 fade" id="nav-contact-05" role="tabpanel" aria-labelledby="nav-contact-03-tab">
                                    People & Sharing
                                </div>
                                <div className="tab-pane lg-product_tab-pane lg-product-col-2 fade" id="nav-contact-06" role="tabpanel" aria-labelledby="nav-contact-03-tab">
                                    Payments & Subscriptions
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Account;