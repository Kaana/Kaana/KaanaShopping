import React from 'react';
import Product from '../../containers/Product/product';
const Saved = () => {
    return (
        <div>
            <div className='container'>
                <div>
                    <div className="row g-5 align-items-center mt-3">
                        <Product />
                        <Product />
                        <Product />
                        <Product />
                        <Product />
                        <Product />
                        <Product />
                        <Product />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Saved;