import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useState } from 'react';
import validEmailRegex from '../../containers/emailRegex';
import { Link } from 'react-router-dom';
import { register } from '../../Redux/actions/auth';
import { Store } from 'react-notifications-component';

const Register = () => {
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [retypepassword, setRetype] = useState("");
    const [successful, setSuccessful] = useState();
    const onChangeValue = (e) => {
        const { name, value } = e.target;
        switch (name) {
            case 'username':
                setUsername(value);
                break;
            case 'email':
                setEmail(value);
                break;
            case 'password':
                setPassword(value);
                break;
            case 'retypepassword':
                setRetype(value);
            default:
                break;
        }
    }


    const { message } = useSelector((state) => state.message);
    console.log(message);
    const dispatch = useDispatch();
    const handleRegister = () => {
        if (username.length < 3) {
            Store.addNotification({
                title: "Error!",
                message: "Enter your Name again.",
                type: "danger",
                insert: "top",
                container: "top-right",
                animationIn: ["animate__animated", "animate__fadeIn"],
                animationOut: ["animate__animated", "animate__fadeOut"],
                dismiss: {
                  duration: 5000,
                  onScreen: true
                }
              });
        } else if (validEmailRegex.test(email) == false) {
            Store.addNotification({
                title: "Error!",
                message: "Enter your Email again.",
                type: "danger",
                insert: "top",
                container: "top-right",
                animationIn: ["animate__animated", "animate__fadeIn"],
                animationOut: ["animate__animated", "animate__fadeOut"],
                dismiss: {
                  duration: 5000,
                  onScreen: true
                }
              });
        } else if (password.length < 8) {
            Store.addNotification({
                title: "Error!",
                message: "Password must more than 8.",
                type: "danger",
                insert: "top",
                container: "top-right",
                animationIn: ["animate__animated", "animate__fadeIn"],
                animationOut: ["animate__animated", "animate__fadeOut"],
                dismiss: {
                  duration: 5000,
                  onScreen: true
                }
              });
        } else if (password !== retypepassword) {
            Store.addNotification({
                title: "Error!",
                message: "Enter your Password again.",
                type: "danger",
                insert: "top",
                container: "top-right",
                animationIn: ["animate__animated", "animate__fadeIn"],
                animationOut: ["animate__animated", "animate__fadeOut"],
                dismiss: {
                  duration: 5000,
                  onScreen: true
                }
              });
        } else {
            dispatch(register(username, email, password))
            
                    console.log(message)
                    // if (message == 'YES') {
                    //     setUsername('');
                    //     setEmail('');
                    //     setPassword('');
                    //     setRetype('');
                    //     alert('Success!');
                       
                    // } else if (message == 'NO') {
                    //     alert('Your email already exist');
                    // }
               
        }
    }
    return (
        <div>
            <div className="login-area rn-section-gapTop">
                <div className="container">
                    <div className="row g-5">
                        <div className="offset-2 col-lg-4 col-md-6 ml_md--0 ml_sm--0 col-sm-12">
                            <div className="form-wrapper-one registration-area">
                                <h4>Sign up</h4>
                                <form>
                                    <div className="mb-3">
                                        <label htmlFor="name" className="form-label">First name</label>
                                        <input type="text"  value={username} name='username' onChange={onChangeValue} />
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="Email" className="form-label">Email</label>
                                        <input type="email" value={email} name="email" onChange={onChangeValue}/>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="passowrd" className="form-label">Password</label>
                                        <input type="password" value={password} name="password" onChange={onChangeValue}/>
                                    </div>
                                    <div className="mb-5">
                                        <label htmlFor="retypepassword" className="form-label">Retypepassword</label>
                                        <input type="password" value={retypepassword} name='retypepassword' onChange={onChangeValue}/>
                                    </div>
                                   
                                    <button type="button" onClick={handleRegister} className='btn btn-info mx-3'>Sign Up</button>
                                    <Link to="/login" className="btn btn-primary-alta">Log In</Link>
                                </form>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-12">
                            <div className="social-share-media form-wrapper-one">
                                <h6>Another way to sign up</h6>
                                <p> Lorem ipsum dolor sit, amet sectetur adipisicing elit.cumque.</p>
                                <button className="another-login login-facebook"> <img className="small-image" src="assets/images/icons/google.png" alt="" /> <span>Log in with Google</span></button>

                                {/* <button className="another-login login-facebook"> <img className="small-image" src="assets/images/icons/facebook.png" alt="" /> <span>Log in with Facebook</span></button>
                                <button className="another-login login-twitter"> <img className="small-image" src="assets/images/icons/tweeter.png" alt="" /> <span>Log in with Twitter</span></button>
                                <button className="another-login login-linkedin"> <img className="small-image" src="assets/images/icons/linkedin.png" alt="" /> <span>Log in with linkedin</span></button> */}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}
export default Register;