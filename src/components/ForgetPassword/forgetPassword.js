import React from 'react';
import { Link } from 'react-router-dom';
const ForgetPassword = () => {
    return (
        <>
            <div className="forget-password-area rn-section-gapTop">
                <div className="container">
                    <div className="row g-5">
                        <div className="offset-4 col-lg-4">
                            <div className="form-wrapper-one">
                                <div className="logo-thumbnail logo-custom-css mb--50">
                                    <Link to="/"><h4>Forget Password?</h4></Link>
                                </div>
                                <div className="mb-5">
                                    <label for="exampleInputEmail1" className="form-label">Email address</label>
                                    <input type="email" id="exampleInputEmail1" placeholder="Enter your email" />
                                </div>
                                <div className="mb-5">
                                    <input type="checkbox" className="rn-check-box-input" id="exampleCheck1" />
                                    <label className="rn-check-box-label" for="exampleCheck1">I agree to the <a href="privacy-policy.html">privacy policy</a> </label>
                                </div>

                                <div className="mb-5">
                                    <button className="btn  btn-primary">Send</button>
                                    <Link to='/login' className='btn btn-success mx-3'><button >Login </button></Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default ForgetPassword;