import React from 'react';
import './cart.css';
const Cart = () => {
    return (
        <>
            <div>
                <div className='text-rigth'>
                    <div className="form-check m-5 mr--50" style={{ 'textAlign': 'right' }}>
                        <h6>All Price : 300$ <button className='btn btn-success ml-2'>Buy</button></h6>

                    </div>

                </div>

                <div className="box-table table-responsive">
                    <table className="table upcoming-projects">
                        <thead>
                            <tr>
                                <th>
                                    <span>SL</span>
                                </th>
                                <th>
                                    <span>Product</span>
                                </th>
                                <th>
                                    <span>Product Name</span>
                                </th>
                                <th>
                                    <span>Price</span>
                                </th>
                                <th>
                                    <span>Amount</span>
                                </th>
                                <th>
                                    <span>Action</span>
                                </th>

                            </tr>
                        </thead>
                        <tbody className="ranking">
                            <tr className="color-light">
                                <td><span>1.</span></td>
                                <td>
                                    <div className="product-wrapper d-flex align-items-center">
                                        <a href="product-details.html" className="thumbnail">
                                            <img src="assets/images/portfolio/portfolio-05.jpg" alt="Nft_Profile" />
                                        </a>
                                        <span>Secure 25</span>
                                    </div>
                                </td>
                                <td><span >Book</span></td>
                                <td><span className="color-danger">200$</span></td>
                                <td><span className="color-green">2</span></td>
                                <td><span><button className='btn btn-danger'>
                                    <i className='fa fa-remove'></i>
                                </button></span></td>

                            </tr>
                            <tr>
                                <td><span>2.</span></td>
                                <td>
                                    <div className="product-wrapper d-flex align-items-center">
                                        <a href="product-details.html" className="thumbnail">
                                            <img src="assets/images/portfolio/portfolio-06.jpg" alt="Nft_Profile" />
                                        </a>
                                        <span>Secure 25</span>
                                    </div>
                                </td>
                                <td><span >Book</span></td>
                                <td><span className="color-green">32$</span></td>
                                <td><span className="color-danger">1</span></td>
                                <td><span><button className='btn btn-danger'>
                                    <i className='fa fa-remove'></i>
                                </button></span></td>

                            </tr>

                            <tr>
                                <td><span>3.</span></td>
                                <td>
                                    <div className="product-wrapper d-flex align-items-center">
                                        <a href="product-details.html" className="thumbnail">
                                            <img src="assets/images/portfolio/portfolio-06.jpg" alt="Nft_Profile" />
                                        </a>
                                        <span>Secure 25</span>
                                    </div>
                                </td>
                                <td><span >Book</span></td>
                                <td><span className="color-green">32$</span></td>
                                <td><span className="color-danger">1</span></td>
                                <td><span><button className='btn btn-danger'>
                                    <i className='fa fa-remove'></i>
                                </button></span></td>

                            </tr>

                            <tr>
                                <td><span>4.</span></td>
                                <td>
                                    <div className="product-wrapper d-flex align-items-center">
                                        <a href="product-details.html" className="thumbnail">
                                            <img src="assets/images/portfolio/portfolio-06.jpg" alt="Nft_Profile" />
                                        </a>
                                        <span>Secure 25</span>
                                    </div>
                                </td>
                                <td><span >Book</span></td>
                                <td><span className="color-green">32$</span></td>
                                <td><span className="color-danger">1</span></td>
                                <td><span><button className='btn btn-danger'>
                                    <i className='fa fa-remove'></i>
                                </button></span></td>

                            </tr>
                            <tr>
                                <td><span>5.</span></td>
                                <td>
                                    <div className="product-wrapper d-flex align-items-center">
                                        <a href="product-details.html" className="thumbnail">
                                            <img src="assets/images/portfolio/portfolio-06.jpg" alt="Nft_Profile" />
                                        </a>
                                        <span>Secure 25</span>
                                    </div>
                                </td>
                                <td><span >Book</span></td>
                                <td><span className="color-green">32$</span></td>
                                <td><span className="color-danger">1</span></td>
                                <td><span><button className='btn btn-danger'>
                                    <i className='fa fa-remove'></i>
                                </button></span></td>

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    )

};

export default Cart;