import React from 'react';
import { Link } from 'react-router-dom';
import Product from '../../containers/Product/product';
import Product1 from '../../containers/Product/product1';
import FavouriteStore from '../../containers/Store/faveriteStore';
import Store from '../../containers/Store/store';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel } from 'react-responsive-carousel';
import './home.css';
const Home = () => {
    return (
        <>
            <div>
                <Carousel autoPlay labels infiniteLoop transitionTime={1000} showStatus={false} showIndicators={false} showThumbs={false} ariaLabel emulateTouch dynamicHeight>
                    <div style={{ 'backgroundSize': 'cover' }}>
                        <img src='assets/images/product/1.jpg' />
                    </div>
                    <div style={{ 'backgroundSize': 'cover' }}>
                        <img src='assets/images/product/2.jpg' />
                    </div>
                    <div style={{ 'backgroundSize': 'cover' }} >
                        <img src='assets/images/product/3.jpg' />
                    </div>

                </Carousel>
                <div className="slider-style-5  my-4 headerpart">
                    <div className="container">
                        <div className="row g-5 align-items-center">
                            <div>
                                <div className="banner-left-content">
                                    <span className="title-badge"  >Kaana Marketplace</span>
                                    <h1 className="title">We welcome you back! <br /> </h1>
                                    <h2 className='titile'>Let 's going shopping</h2>
                                    <p className="banner-disc-one">

                                    </p>
                                    <div className="button-group">
                                        <Link className="btn btn-large btn-primary buttonColor" to="/login">Login</Link>
                                        <Link className="btn btn-large btn-primary-alta" to="/register">Register</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="rn-service-area rn-section-gapTop">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 ">
                                <h3 className="title" >Favourite Stores</h3>
                            </div>
                        </div>
                        <hr />
                        <div className="slider-style-5 ">
                            <div className="row g-5 align-items-center">

                                <FavouriteStore />

                                <FavouriteStore />

                                <FavouriteStore />

                                <FavouriteStore />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="rn-product-area rn-section-gapTop">
                    <div className="container">
                        <div className="row mb--30 align-items-center">
                            <div className="col-12">
                                <h3 className="title mb--0" >Recently Seen</h3>
                            </div>
                        </div>
                        <hr />
                        <div className="slider-style-5 ">
                            <div className="row g-5 align-items-center">
                                <Product />
                                <Product />
                                <Product />
                                <Product />
                                <Product />
                                <Product />
                                <Product />
                                <Product />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="rn-collection-area rn-section-gapTop">
                    <div className="container">
                        <div className="row mb--25 align-items-center">
                            <div className="col-lg-6 col-md-6 col-sm-6 col-12">
                                <h3 className="title mb--0" >Popular Product</h3>
                            </div>
                        </div>
                        <hr />
                        <div className="slider-style-5 ">
                            <div className="row g-5 align-items-center">
                                <Product />
                                <Product />
                                <Product />
                                <Product />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12" >
                                <nav className="pagination-wrapper" aria-label="Page navigation example">
                                    <ul className="pagination">
                                        <li className="page-item"><a className="page-link" href='#'>Previous</a></li>
                                        <li className="page-item"><a className="page-link active" href="#">1</a></li>
                                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                                        <li className="page-item"><a className="page-link" href="#">Next</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="rn-top-top-seller-area nice-selector-transparent rn-section-gapTop">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <h3 className="title">Stores</h3>
                            </div>
                        </div>
                        <hr />
                        <div className="slider-style-5 ">
                            <div className="row g-5 align-items-center">
                                <Store />
                                <Store />
                                <Store />
                                <Store />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}
export default Home;