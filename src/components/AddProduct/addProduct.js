import React from 'react';
import { Link } from 'react-router-dom';
const AddProduct = () => {
    return (
        <>
            <div>
                <div className="create-area rn-section-gapTop">
                    <div className="container-fluid">
                        <div className="row g-5">
                            <div className="col-lg-4 offset-1 ml_md--0 ml_sm--0">
                                <div className="upload-area">

                                    <div className="upload-formate mb--30">
                                        <h6 className="title">
                                            Upload file
                                        </h6>
                                        <p className="formate">
                                            Drag or choose your file to upload
                                        </p>
                                    </div>

                                    <div className="brows-file-wrapper">
                                        <input name="createinputfile" id="createinputfile" type="file" className="inputfile" />
                                        <label htmlFor="createinputfile" title="No File Choosen">
                                            <i className="feather-upload"></i>
                                            <span className="text-center">Choose a File</span>
                                            <p className="text-center mt--10">PNG, GIF, WEBP, MP4. <br />    Max 1Gb.</p>
                                        </label>
                                    </div>
                                </div>

                                <div className="mt--100 mt_sm--30 mt_md--30 d-none d-lg-block">
                                    <table className='table table-bordered text-center table-hover text-white'>
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Data Name</th>
                                                <th>Data Type</th>
                                                <th>Size</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div className="col-lg-6">
                                <div className="form-wrapper-one">
                                    <form className="row" action="#">

                                        <div className="col-md-12">
                                            <div className="input-box pb--20">
                                                <label htmlFor="name" className="form-label">Product Title</label>
                                                <input name='productTitle' placeholder="Enter your Product Title" />
                                            </div>
                                        </div>

                                        <div className="col-md-12">
                                            <div className="input-box pb--20">
                                                <label htmlFor="Discription" className="form-label">Discription</label>
                                                <textarea name='discription' rows="3" placeholder="e. g. “After purchasing the product you can get item...”"></textarea>
                                            </div>
                                        </div>

                                        <div className="col-md-4">
                                            <div className="input-box pb--20">
                                                <label htmlFor="dollerValue" className="form-label">Condition</label>
                                                <select className='p-3 ' name='condition'>
                                                    <option>Enter Product Condition</option>
                                                    <option value={1}>1</option>
                                                    <option value={2}>2</option>
                                                    <option value={3}>3</option>
                                                    <option value={4}>4</option>
                                                    <option value={5}>5</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div className="col-md-4">
                                            <div className="input-box pb--20">
                                                <label htmlFor="dollerValue" className="form-label">Department</label>
                                                <select className='p-3' name='department'>
                                                    <option>Enter Product Department</option>
                                                    <option value={1}>1</option>
                                                    <option value={2}>2</option>
                                                    <option value={3}>3</option>
                                                    <option value={4}>4</option>
                                                    <option value={5}>5</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div className="col-md-4">
                                            <div className="input-box pb--20">
                                                <label htmlFor="price" className="form-label">Item Price in $</label>
                                                <input name='price' placeholder="Enter Product Price" />
                                            </div>
                                        </div>

                                        <div className="col-md-4">
                                            <div className="input-box pb--20">
                                                <label htmlFor="Psropertie" className="form-label">Size</label>
                                                <input name='size' placeholder="Enter Product Size" />
                                            </div>
                                        </div>



                                        <div className="col-md-4">
                                            <div className="input-box pb--20">
                                                <label htmlFor="Size" className="form-label">Tags</label>
                                                <input name='tags' placeholder="Enter Product Tags" />
                                            </div>
                                        </div>

                                        <div className="col-md-4">
                                            <div className="input-box pb--20">
                                                <label htmlFor="Propertie" className="form-label">Propertie</label>
                                                <input name='propertie' placeholder="Enter Product Propertie" />
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="input-box pb--20">
                                                <label htmlFor="Propertie" className="form-label">Brand</label>
                                                <input name='brand' placeholder="Enter Product Brand" />
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="input-box pb--20">
                                                <label htmlFor="Propertie" className="form-label">Stock</label>
                                                <input name='stock' placeholder="Enter Product Stock" />
                                            </div>
                                        </div>
                                        <h3>Shipping</h3>
                                        <h6>Ships from</h6>
                                        <div className="col-md-12">
                                            <div className="input-box pb--20">
                                                <label htmlFor="postalCode" className="form-label">Enter Product PostalCode</label>
                                                <input name='postalCode' placeholder="Postal Code" />
                                            </div>
                                        </div>
                                        <span>Delivery</span>
                                        <span>Sell to buyers across the world. We'll email you a label and you 'll ship the item. you get to pick who pays for shipping.</span>
                                        <div className="col-md-12">
                                            <div className="input-box pb--20">
                                                <label htmlFor="delivery" className="form-label">Shipping</label>
                                                <input name='delivery' placeholder="Enter Product Delivery" />
                                            </div>
                                        </div>
                                        <h6>Smart pricing</h6>
                                        <h6>Set it and forget it. Increase your chances of a sale.</h6>

                                        <div className="col-md-12 col-xl-4">
                                            <div className="input-box">
                                                <button className="btn btn-primary-alta btn-large w-100" data-bs-toggle="modal" data-bs-target="#uploadModal">Preview</button>
                                            </div>
                                        </div>

                                        <div className="col-md-12 col-xl-8 mt_lg--15 mt_md--15 mt_sm--15">
                                            <div className="input-box">
                                                <button type='button' className="btn btn-primary btn-large w-100">
                                                    <i className='fa fa-save mx-2'></i>
                                                    Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>

                            <div className="mt--100 mt_sm--30 mt_md--30 d-block d-lg-none">
                                <h5> Note: </h5>
                                <span> Service fee : <strong>2.5%</strong> </span> <br />
                                <span> You will receive : <strong>25.00 ETH $50,000</strong></span>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="rn-popup-modal upload-modal-wrapper modal fade" id="uploadModal" tabIndex="-1" aria-hidden="true">
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"><i data-feather="x"></i></button>
                    <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                        <div className="modal-content share-wrapper">
                            <div className="modal-body">
                                <div className="product-style-one no-overlay">
                                    <div className="card-thumbnail">
                                        <a href="product-details.html"><img src="assets/images/portfolio/portfolio-05.jpg" alt="NFT_portfolio" /></a>
                                    </div>
                                    <div className="product-share-wrapper">
                                        <div className="profile-share">
                                            <a href="author.html" className="avatar" data-tooltip="Jone lee"><img src="assets/images/client/client-1.png" alt="Nft_Profile" /></a>
                                            <a href="author.html" className="avatar" data-tooltip="Jone lee"><img src="assets/images/client/client-2.png" alt="Nft_Profile" /></a>
                                            <a href="author.html" className="avatar" data-tooltip="Jone lee"><img src="assets/images/client/client-3.png" alt="Nft_Profile" /></a>
                                            <a className="more-author-text" href="#">9+ Place Bit.</a>
                                        </div>
                                        <div className="share-btn share-btn-activation dropdown">

                                            <div className="share-btn-setting dropdown-menu dropdown-menu-end">
                                                <button type="button" className="btn-setting-text share-text" data-bs-toggle="modal" data-bs-target="#shareModal">
                                                    Share
                                                </button>
                                                <button type="button" className="btn-setting-text report-text" data-bs-toggle="modal" data-bs-target="#reportModal">
                                                    Report
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                    <a href="product-details.html"><span className="product-name">Preatent</span></a>
                                    <span className="latest-bid">Highest bid 1/20</span>
                                    <div className="bid-react-area">
                                        <div className="last-bid">0.244wETH</div>
                                        <div className="react-area">
                                            <svg viewBox="0 0 17 16" fill="none" width="16" height="16" className="sc-bdnxRM sc-hKFxyN kBvkOu">
                                                <path d="M8.2112 14L12.1056 9.69231L14.1853 7.39185C15.2497 6.21455 15.3683 4.46116 14.4723 3.15121V3.15121C13.3207 1.46757 10.9637 1.15351 9.41139 2.47685L8.2112 3.5L6.95566 2.42966C5.40738 1.10976 3.06841 1.3603 1.83482 2.97819V2.97819C0.777858 4.36443 0.885104 6.31329 2.08779 7.57518L8.2112 14Z" stroke="currentColor" strokeWidth="2"></path>
                                            </svg>
                                            <span className="number">322</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default AddProduct;
