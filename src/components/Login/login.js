import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import validEmailRegex from '../../containers/emailRegex';
import { login } from '../../Redux/actions/auth';
const Login = (e) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const inputValue = (e) => {
        const { name, value } = e.target;
        switch (name) {
            case 'email':
                setEmail(value);
                break;
            case 'password':
                setPassword(value);
                break;
            default:
                break;
        }
    }
    const { isLoggedIn } = useSelector(state => state.auth);
    const { message } = useSelector(state => state.message);
    const dispatch = useDispatch();
    const submitvalue = () => {
        if (validEmailRegex.test(email) == false) {
            alert('your email error!')
        } else if (password.length < 8) {
            alert('your password error!');
        } else {
            dispatch(login(email, password));
            console.log(message);
            if (message == 'NO') {
                alert('Your Email isn\'t exist')
            } else if (isLoggedIn == true) {
                alert('success');
            }
        }
    }
    return (
        <>
            <div className="login-area rn-section-gapTop">
                <div className="container">
                    <div className="row g-5">
                        <div className=" offset-2 col-lg-4 col-md-6 ml_md--0 ml_sm--0 col-sm-12">
                            <div className="form-wrapper-one">
                                <h4>Login</h4>
                                <form>
                                    <div className="mb-5">
                                        <label className="form-label">Email address</label>
                                        <input type="email" name='email' onChange={inputValue} />
                                    </div>
                                    <div className="mb-5">
                                        <label className="form-label">Password</label>
                                        <input type="password" name='password' onChange={inputValue} />
                                    </div>
                                    <div className="mb-5 rn-check-box">
                                        <input type="checkbox" className="rn-check-box-input" />
                                        <label className="rn-check-box-label" htmlFor="exampleCheck1">Remember me leter</label>
                                    </div>
                                    <div className='mb-5'>
                                        <Link to='/forgetPassword'>Forget password</Link>
                                    </div>
                                    <button type="button" onClick={submitvalue} className="btn btn-primary mr--15">Log In</button>
                                    <Link to="/register" className="btn btn-primary-alta">Sign Up</Link>
                                </form>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-12">
                            <div className="social-share-media form-wrapper-one">
                                <h6>Another way to log in</h6>
                                <p> Lorem ipsum dolor sit, amet sectetur adipisicing elit.cumque.</p>
                                <button className="another-login login-facebook"> <img className="small-image" src="assets/images/icons/google.png" alt="" /> <span>Log in with Google</span></button>
                                <button className="another-login login-facebook"> <img className="small-image" src="assets/images/icons/facebook.png" alt="" /> <span>Log in with Facebook</span></button>
                                <button className="another-login login-twitter"> <img className="small-image" src="assets/images/icons/tweeter.png" alt="" /> <span>Log in with Twitter</span></button>
                                <button className="another-login login-linkedin"> <img className="small-image" src="assets/images/icons/linkedin.png" alt="" /> <span>Log in with linkedin</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Login;