import React from 'react';
import Product from '../../containers/Product/product';
import './search.css';
const Search = () => {
    return (
        <>
            <div className='container'>
                <div className='text-center m-4'>
                    <select className='col-lg-3' style={{ 'padding': '8.5px' }}>
                        <option>Category Here</option>
                        <option>All</option>
                        <option>option2</option>
                        <option>option3</option>
                        <option>option4</option>
                        <option>option5</option>
                        <option>option6</option>
                    </select>
                    <input type="search" className='col-lg-3' placeholder="Search Product Name" aria-label="Search" />
                    <button className="searchbtn" >
                        <i className='fa fa-search'></i>
                    </button>
                </div>
                <hr />
                <div>
                    <div className="row g-5 align-items-center mt-3">
                        <Product />
                        <Product />
                        <Product />
                        <Product />
                        <Product />
                        <Product />
                        <Product />
                        <Product />
                    </div>
                </div>
            </div>
        </>
    )
}

export default Search;