import React from 'react';
import './sidelink.css';
import { Link } from 'react-router-dom';
const SideLink = () => {
    return (
        <>
            <div className='btn-group-vertical'>
                <Link to='/search' className='btn btn-info' ><i className='fa fa-search' title="Hooray!"></i></Link>
                <Link to='/saved' className='btn btn-info'><i className='fa fa-save' title='Saved'></i></Link>
                <Link to='/addProduct' className='btn btn-info'><i className='fa fa-plus-circle' title='Add Product'></i></Link>
                <Link to='/profile' className='btn btn-info'><i className='fa fa-user' title='Profile'></i></Link>
                <Link to='/cart' className='btn btn-info'><i className='fa fa-cart-arrow-down' title='Cart'></i></Link>
            </div>
        </>
    )
}
export default SideLink;