import React from 'react';
const ProductDetail = () => {
    return (
        <div>
            <div className="product-details-area rn-section-gapTop">
                <div className="container">
                    <div className="row g-5">

                        <div className="col-lg-7 col-md-12 col-sm-12">
                            <div className="product-tab-wrapper rbt-sticky-top-adjust">
                                <div className="pd-tab-inner">
                                    <div className="nav rn-pd-nav rn-pd-rt-content nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                        <button className="nav-link active" id="v-pills-home-tab" data-bs-toggle="pill" data-bs-target="#v-pills-home" type="button" role="tab" aria-controls="v-pills-home" aria-selected="true">
                                            <span className="rn-pd-sm-thumbnail">
                                            <img src="assets/images/portfolio/sm/portfolio-01.jpg" alt="Nft_Profile" />
                                            </span>
                                        </button>
                                        <button className="nav-link" id="v-pills-profile-tab" data-bs-toggle="pill" data-bs-target="#v-pills-profile" type="button" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                                            <span className="rn-pd-sm-thumbnail">
                                            <img src="assets/images/portfolio/sm/portfolio-01.jpg" alt="Nft_Profile" />
                                            </span>
                                        </button>
                                        <button className="nav-link" id="v-pills-messages-tab" data-bs-toggle="pill" data-bs-target="#v-pills-messages" type="button" role="tab" aria-controls="v-pills-messages" aria-selected="false">
                                            <span className="rn-pd-sm-thumbnail">
                                            <img src="assets/images/portfolio/sm/portfolio-01.jpg" alt="Nft_Profile" />
                                            </span>
                                        </button>
                                    </div>
                                    <div className="tab-content rn-pd-content" id="v-pills-tabContent">
                                        <div className="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                            <div className="rn-pd-thumbnail">
                                            <img src="assets/images/portfolio/sm/portfolio-01.jpg" alt="Nft_Profile" />
                                            </div>
                                        </div>
                                        <div className="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                            <div className="rn-pd-thumbnail">
                                            <img src="assets/images/portfolio/sm/portfolio-01.jpg" alt="Nft_Profile" />
                                            </div>
                                        </div>
                                        <div className="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                            <div className="rn-pd-thumbnail">
                                            <img src="assets/images/portfolio/sm/portfolio-01.jpg" alt="Nft_Profile" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div className="col-lg-5 col-md-12 col-sm-12 mt_md--50 mt_sm--60">
                            <div className="rn-pd-content-area">
                                <div className="pd-title-area">
                                    <h3 className="title">Computer (33$)</h3>
                                    <div className="pd-react-area">
                                        <div className="heart-count">
                                            <i data-feather="heart"></i>
                                            <span>33</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="catagory-collection">
                                    <div className="catagory">
                                        <span>Catagory <span className="color-body"></span></span>
                                        <div className="top-seller-inner-one">
                                            <div className="top-seller-wrapper">
                                                <div className="thumbnail">
                                                    <a href="#"><img src="assets/images/client/client-1.png" alt="Nft_Profile" /></a>
                                                </div>
                                                <div className="top-seller-content">
                                                    <a href="#">
                                                        <h6 className="name">Brodband</h6>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="collection">
                                        <span>Collections</span>
                                        <div className="top-seller-inner-one">
                                            <div className="top-seller-wrapper">
                                                <div className="thumbnail">
                                                    <a href="#"><img src="assets/images/client/client-2.png" alt="Nft_Profile" /></a>
                                                </div>
                                                <div className="top-seller-content">
                                                    <a href="#">
                                                        <h6 className="name">Brodband</h6>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="rn-bid-details">
                                    <div className="tab-wrapper-one">
                                        <nav className="tab-button-one">
                                            <div className="nav nav-tabs" id="nav-tab" role="tablist">
                                                <button className="nav-link" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="false">Seller Information</button>
                                                <button className="nav-link active" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="true">Product Introduction</button>
                                                <button className="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">History</button>
                                            </div>
                                        </nav>
                                        <div className="tab-content rn-bid-content" id="nav-tabContent">
                                            <div className="tab-pane fade" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                                <div className="top-seller-inner-one">
                                                    <div className="top-seller-wrapper">
                                                        {/* <div className="thumbnail">
                                                                <a href="#"><img src="assets/images/client/client-3.png" alt="Nft_Profile" /></a>
                                                            </div>
                                                            <div className="top-seller-content">
                                                                <span> Ukrainee<a href="#">Allen Waltker</a></span>
                                                                <span className="count-number">
                                                                    1 hours ago
                                                                </span>
                                                            </div> */}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="tab-pane fade show active" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                                                <div className="rn-pd-bd-wrapper">
                                                    <div className="top-seller-inner-one">
                                                        <h6 className="name-title">

                                                        </h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                                <div className="top-seller-inner-one mt--20">
                                                    <div className="top-seller-wrapper">
                                                        <div className="thumbnail">
                                                            <a href="#"><img src="assets/images/client/client-5.png" alt="Nft_Profile" /></a>
                                                        </div>
                                                        <div className="top-seller-content">
                                                            <span><a href="#">Allen Waltker</a></span>
                                                            <span><a href="#">(Ukrainee)</a></span>
                                                            <span className="count-number">
                                                                Amonut : 1 &nbsp;  paid : 33$
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="top-seller-inner-one mt--20">
                                                    <div className="top-seller-wrapper">
                                                        <div className="thumbnail">
                                                            <a href="#"><img src="assets/images/client/client-5.png" alt="Nft_Profile" /></a>
                                                        </div>
                                                        <div className="top-seller-content">
                                                            <span><a href="#">Allen Waltker</a></span>
                                                            <span><a href="#">(Ukrainee)</a></span>
                                                            <span className="count-number">
                                                                Amonut : 1 &nbsp;  paid : 33$
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="place-bet-area">
                                        <div className="rn-bet-create">
                                        </div>
                                        <button type="button" className="btn btn-primary-alta mt--30" data-bs-toggle="modal" data-bs-target="#placebidModal">Buy</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProductDetail;