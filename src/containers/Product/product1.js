
import React from 'react';

import Product2 from './product2.js';
const Product1 = () => {
    return (
        <>
            <div id="demo" className="carousel slide" data-bs-ride="carousel">

                <div className="carousel-indicators">
                    <button type="button" data-bs-target="#demo" data-bs-slide-to="0" className="active"></button>
                    <button type="button" data-bs-target="#demo" data-bs-slide-to="1"></button>
                    <button type="button" data-bs-target="#demo" data-bs-slide-to="2"></button>
                    <button type="button" data-bs-target="#demo" data-bs-slide-to="3"></button>
                    <button type="button" data-bs-target="#demo" data-bs-slide-to="4"></button>
                </div>
                <div className="carousel-inner col-lg-12">
                    <div className="carousel-item  active">
                        <Product2 />
                    </div>
                    <div className="carousel-item">
                        <Product2 />
                    </div>
                    <div className="carousel-item">
                        <Product2 />
                    </div>
                    <div className="carousel-item">
                        <Product2 />
                    </div>
                    <div className="carousel-item">
                        <Product2 />
                    </div>
                    
                </div>
                <button className="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
                    <span className="carousel-control-prev-icon"></span>
                </button>
                <button className="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
                    <span className="carousel-control-next-icon"></span>
                </button>
            </div>

        </>
    )
}

export default Product1; 