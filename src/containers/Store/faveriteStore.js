import React from 'react';
import { Link } from 'react-router-dom';

const FavouriteStore = () => {
    return (
        <div className="col-lg-4 col-xl-3 col-md-6 col-sm-6 col-12">
            <Link to='/storeProducts' className="rn-collection-inner-one">
                <div className="collection-wrapper">
                    <div className="collection-big-thumbnail">
                        <img src="assets/images/collection/collection-lg-01.jpg" alt="Nft_Profile" />
                    </div>
                    <div className="collenction-small-thumbnail">
                        <img src="assets/images/collection/collection-lg-01.jpg" alt="Nft_Profile" />
                        <img src="assets/images/collection/collection-lg-01.jpg" alt="Nft_Profile" />
                        <img src="assets/images/collection/collection-lg-01.jpg" alt="Nft_Profile" />
                    </div>
                    <div className="collection-profile">
                        <img src="assets/images/client/client-15.png" alt="Nft_Profile" />
                    </div>
                    <div className="collection-deg">
                        <h6 className="title">Jacks</h6>
                        <span className="items">27 Items</span>
                    </div>
                </div>
            </Link>
        </div>
    )
}


export default FavouriteStore;