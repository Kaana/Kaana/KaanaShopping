import React from 'react';
import { Link } from 'react-router-dom';
const Store = () => {

    return (
        <div className="col-xxl-3 col-lg-3 col-md-6 col-sm-6 col-12">
            <div className="rn-service-one color-shape-7">
                <div className="inner">
                    <div className="icon">
                        <img src="assets/images/icons/shape-1.png" alt="Shape" />
                    </div>
                    <div className="subtitle">Store-01</div>
                    <div className="content">
                        <h4 className="title"><Link to='/storeProducts'>Tom 's Store</Link></h4>
                        <p className="description">Powerful features and inclusions, which makes  standout,
                            easily customizable and scalable.</p>
                        <a className="read-more-button" href='/storeProducts'><i className="feather-arrow-right"></i></a>
                    </div>
                </div>
                <Link className="over-link" to='/storeProducts'></Link>
            </div>
        </div>
    )
}
export default Store;


