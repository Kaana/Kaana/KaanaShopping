import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import './nav.css';

class Nav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mobilestyle: 'popup-mobile-menu'
        }
    }


    navbar = () => {
        this.setState({ mobilestyle: 'popup-mobile-menu active' })

    }
    close = () => {
        this.setState({ mobilestyle: 'popup-mobile-menu' })
    }
    backgroundChange = () => {
        document.getElementById('body').style.backgroundColor = '#663300'
        document.getElementById('productModel').style.backgroundColor = '#663300'
    }
    render() {
        return (
            <div>
                <header className="rn-header haeder-default black-logo-version header--fixed header--sticky">
                    <div className="container">
                        <div className="header-inner">
                            <div className="header-left">
                                <div className="logo-thumbnail logo-custom-css">
                                    <Link className="logo-light" to="/"><img src='assets/images/shopping.png' alt="nft-logo" width="50px" className='mb-4' /><span className='m-2' style={{ 'fontSize': '190%' }}>Kaana Shopping</span></Link>
                                </div>
                                <div className="mainmenu-wrapper">
                                    <nav id="sideNav" className="mainmenu-nav d-none d-xl-block">
                                        <ul className="mainmenu">
                                            <li><NavLink to="/" className='active'>Home</NavLink></li>
                                            <li><NavLink to="contact" className='active'>Contact</NavLink></li>
                                            <li><NavLink to="aboutUs" className='active'>About Us</NavLink></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div className="header-right">
                                <div className="setting-option header-btn rbt-site-header" id="rbt-site-header">
                                    <div className="icon-box">
                                        <Link id="connectbtn" className="btn btn-primary-alta btn-small" to='/connect'>Wallet connect</Link>
                                    </div>
                                </div>
                                <div className="setting-option rn-icon-list notification-badge">
                                    <div className="share-btn share-btn-activation dropdown">
                                        <button className="icon-box noneborder" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            <a href="#"><i className="feather-bell"></i><span className="badge">6</span></a>
                                        </button>

                                        <div className="share-btn-setting text-center dropdown-menu dropdown-menu-end" style={{ 'width': '200px' }}>
                                            <Link to='/productDetail'>
                                                <div className='row p-3'>

                                                    <div className='col-4'>
                                                        <img src="assets/images/client/client-15.png" className='rounded-circle productImage' alt="Nft_Profile" />
                                                    </div>
                                                    <div className='col-8 text-white productAlign'>
                                                        <div className='productName'>Product</div>
                                                        <div className='productPrice'>300£</div>

                                                    </div>
                                                    <div><hr className="dropdown-divider text-white" /></div>
                                                </div>
                                            </Link>
                                            <Link to='/productDetail'>
                                                <div className='row p-3'>

                                                    <div className='col-4'>
                                                        <img src="assets/images/client/client-15.png" className='rounded-circle productImage' alt="Nft_Profile" />
                                                    </div>
                                                    <div className='col-8 text-white productAlign'>
                                                        <div className='productName'>Product</div>
                                                        <div className='productPrice'>300£</div>

                                                    </div>
                                                    <div><hr className="dropdown-divider text-white" /></div>
                                                </div>
                                            </Link>
                                            <Link to='/productDetail'>
                                                <div className='row p-3'>

                                                    <div className='col-4'>
                                                        <img src="assets/images/client/client-15.png" className='rounded-circle productImage' alt="Nft_Profile" />
                                                    </div>
                                                    <div className='col-8 text-white productAlign'>
                                                        <div className='productName'>Product</div>
                                                        <div className='productPrice'>300£</div>

                                                    </div>
                                                    <div><hr className="dropdown-divider text-white" /></div>
                                                </div>
                                            </Link>
                                            <Link to='/productDetail'>
                                                <div className='row p-3'>

                                                    <div className='col-4'>
                                                        <img src="assets/images/client/client-15.png" className='rounded-circle productImage' alt="Nft_Profile" />
                                                    </div>
                                                    <div className='col-8 text-white productAlign'>
                                                        <div className='productName'>Product</div>
                                                        <div className='productPrice'>300£</div>

                                                    </div>
                                                    <div><hr className="dropdown-divider text-white" /></div>
                                                </div>
                                            </Link>
                                            <Link to='/productDetail'>
                                                <div className='row p-3'>

                                                    <div className='col-4'>
                                                        <img src="assets/images/client/client-15.png" className='rounded-circle productImage' alt="Nft_Profile" />
                                                    </div>
                                                    <div className='col-8 text-white productAlign'>
                                                        <div className='productName'>Product</div>
                                                        <div className='productPrice'>300£</div>

                                                    </div>
                                                    <div><hr className="dropdown-divider text-white" /></div>
                                                </div>
                                            </Link>
                                            <Link to='/productDetail'>
                                                <div className='row p-3'>

                                                    <div className='col-4'>
                                                        <img src="assets/images/client/client-15.png" className='rounded-circle productImage' alt="Nft_Profile" />
                                                    </div>
                                                    <div className='col-8 text-white productAlign'>
                                                        <div className='productName'>Product</div>
                                                        <div className='productPrice'>300£</div>
                                                    </div>
                                                </div>
                                            </Link>
                                        </div>
                                    </div>
                                </div>

                                <div className="setting-option mobile-menu-bar d-block d-xl-none">
                                    <div className="hamberger">
                                        <button className="hamberger-button" onClick={this.navbar}>
                                            <i className="feather-menu"></i>
                                        </button>
                                    </div>
                                </div>

                                <div id="my_switcher" className="my_switcher setting-option" onClick={this.backgroundChange}>
                                    <ul>
                                        <li>
                                            <a href="#" className="setColor light">
                                                <img className="sun-image" src='assets/images/icons/sun-01.svg' alt="Sun images" />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <div className={this.state.mobilestyle}>
                    <div className="inner">
                        <div className="header-top">
                            <div className="logo logo-custom-css">
                                <a className="logo-light" href="/"><img src='assets/images/shopping.png' alt="nft-logo" /></a>
                            </div>
                            <div className="close-menu">
                                <button className="close-button" onClick={this.close}>
                                    <i className="feather-x"></i>
                                </button>
                            </div>
                        </div>
                        <nav>
                            <ul className="mainmenu">
                                <li><NavLink to="/" className='active'>Home</NavLink></li>
                                <li><NavLink to="contact" className='active'>Contact</NavLink></li>
                                <li><NavLink to="aboutUs" className='active'>About Us</NavLink></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        )
    }
}
export default Nav;