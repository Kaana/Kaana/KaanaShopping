import React from 'react';
import './footer.css';
const Footer = () => {
    return (
        <>
            <div className="copy-right-one ptb--20 bg-color--1 mt--90">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-lg-6 col-md-12 col-sm-12">
                            <div className="copyright-left">
                                <span>©2022 Kaana Shopping, Inc. All rights reserved.</span>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-12 col-sm-12 copyright-right">
                            <div className="copyright-right col-lg-3 col-md-6 ">
                                <img src='assets/images/googlePlay.png' alt='googlePlay' className='mx-1 col-lg-12 googlePlay' />
                                <img src='assets/images/appStore.png' alt='appStore' className='mx-1 col-lg-12 appStore' />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Footer;