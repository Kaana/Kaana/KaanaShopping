import './index.css';
import 'font-awesome/css/font-awesome.css';
// import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
//import react-router-dom module
import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom';

//import the components
import Home from './components/Home/home';
import Header from './containers/Header/header';
import Footer from './containers/Footer/footer';
import Login from './components/Login/login';
import Register from './components/Register/register';
import ForgetPassword from './components/ForgetPassword/forgetPassword';
import AddProduct from './components/AddProduct/addProduct';
import Contact from './components/Contact/contact';
import AboutUs from './components/AboutUs/aboutUs';
import ProductDetail from './components/ProductDetail/productDetail';
import WalletConnect from './components/WalletConnect/walletConnect';
import SideLink from './components/SideLink/sidelink';
import Saved from './components/Saved/saved';
import Search from './components/Search/search';
import Cart from './components/Cart/cart';
import StoreProducts from './components/StoreProducts/storeProducts';
import Account from './components/Account/account';
import Profile from './components/Profile/profile';
import { history } from "./Redux/helpers/history";
import { ReactNotifications } from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
const App = () => {
  const dispatch = useDispatch();
  return (
    <>
      <div className='app-container'>
        <ReactNotifications />
        <BrowserRouter history={history}>
          <Header />
          <SideLink />
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/login' element={<Login />} />
            <Route path='/register' element={<Register />} />
            <Route path='/forgetPassword' element={<ForgetPassword />} />
            <Route path='/addProduct' element={<AddProduct />} />
            <Route path='/contact' element={<Contact />} />
            <Route path='/aboutUs' element={<AboutUs />} />
            <Route path='/addProduct' element={<AddProduct />} />
            <Route path='/productDetail' element={<ProductDetail />} />
            <Route path='/connect' element={<WalletConnect />} />
            <Route path='/saved' element={<Saved />} />
            <Route path='/search' element={<Search />} />
            <Route path='/cart' element={<Cart />} />
            <Route path='/storeProducts' element={<StoreProducts />} />
            <Route path='/account' element={<Account />} />
            <Route path='/profile' element={<Profile />} />
          </Routes>
        </BrowserRouter>
        <Footer />
      </div>
    </>
  );
}

export default App;